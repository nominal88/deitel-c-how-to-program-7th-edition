/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.24 - Page 109 - Tabular output.               |
  +----------------------------------------------------------------------+
  | Filename: Ex3_24.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>
#include <limits.h>

static const unsigned int numberOfNumbers = 20;

int main()
{
    printf("N\t10*N\t100*N\t1000*N\r\n");

    for (unsigned int i = 0; i < numberOfNumbers; i++) {
        printf("%u\t%lu\t%lu\t%lu\r\n", i, (unsigned long)10*i, (unsigned long)100*i, (unsigned long)1000*i);
    }

    return 0;
}
