/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.34 - Page 112 - Palindrome tester.            |
  +----------------------------------------------------------------------+
  | Filename: Ex3_34.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>
#include <stdlib.h>


int main()
{
    // Variables
    unsigned int readNumber;
    unsigned short numOfDigits;
    bool isPalindramoVar = false;

    unsigned char *pDigits = NULL;

    // Function prototypes
    unsigned short findNumOfDigits (unsigned int number);
    bool isPalindrome (unsigned char *pDigits, unsigned short numberOfDigits);
    void numberToArray(unsigned int number, unsigned char *pDigits);

    printf ("%s", "Enter a number for palindrome test: ");
    scanf("%u", &readNumber);

    numOfDigits = findNumOfDigits(readNumber);

    pDigits = (unsigned char *) calloc( numOfDigits, sizeof (unsigned char) );

    numberToArray(readNumber,pDigits);

    isPalindramoVar = isPalindrome(pDigits, numOfDigits);

    printf ("%d %s\r\n", readNumber, (isPalindramoVar) ? "is palindrome!" : "is not palindrome!");

    free(pDigits);



    free (pDigits);
    return 0;
}

/* Find the number of digits of the entered number */
unsigned short findNumOfDigits (unsigned int number)
{
    const unsigned short numberSystem = 10;    // Decimal number system
    unsigned short numberOfDigits = 0;

    while (number > 0) {
        numberOfDigits++;
        number /= numberSystem;
    }

    return numberOfDigits;
}

void numberToArray(unsigned int number, unsigned char *pDigits)
{
    unsigned short numberOfDigits = findNumOfDigits(number);

    for (unsigned char i = 0; i < numberOfDigits; i++) {
        pDigits[numberOfDigits - 1 - i] = number % 10;
        number /= 10;
    }
}


/* Check whether the number is palindrome */
bool isPalindrome (unsigned char *pDigits, unsigned short numberOfDigits)
{
    for(unsigned char i = 0; i < numberOfDigits / 2; i++) {
        if (pDigits[i] != pDigits[numberOfDigits - 1 - i]) {
            return false;
        }
    }
    return true;
}
