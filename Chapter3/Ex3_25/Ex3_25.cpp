/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.25 - Page 109 - Tabular output.               |
  +----------------------------------------------------------------------+
  | Filename: Ex3_25.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>
#include <limits.h>

static const unsigned int numberOfNumbers = 100;

int main()
{
    printf("A\tA+2\tA+4\tA+6\r\n");

    for (unsigned int i = 3; i < numberOfNumbers; i += 3) {
        printf("%u\t%u\t%u\t%u\r\n", i, (unsigned int)i+2, (unsigned int)i+4, (unsigned int)i+6);
    }

    return 0;
}
