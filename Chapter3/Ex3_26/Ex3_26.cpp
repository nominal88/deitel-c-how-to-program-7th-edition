/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.26 - Page 109 - Find the largest 2 numbers.   |
  +----------------------------------------------------------------------+
  | Filename: Ex3_26.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>
#include <limits.h>

static const unsigned int numberOfNumbers = 10;

int main()
{
    int actualNumber = 0;
    int largestNumber1 = INT_MIN;
    int largestNumber2 = INT_MIN;
    unsigned int index = 0;

    do {
        printf ("Number[%d]: ", index);
        scanf("%d", &actualNumber);

        if ( actualNumber > largestNumber1 ) {
            largestNumber2 = largestNumber1;
            largestNumber1 = actualNumber;
        }
        else if ( actualNumber > largestNumber2 ) {
            largestNumber2 = actualNumber;
        }


    } while (++index < numberOfNumbers);

    printf("The largest numbers are %d and %d.\r\n", largestNumber1, largestNumber2);

    return 0;
}
