/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.17 - Page 107 - Credit limit calculator       |
  +----------------------------------------------------------------------+
  | Filename: Ex3_17.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>

// Main function - begin
int main ( void )
{
    long int accountNumber;
    float initialBalance, totalExpense, totalDeposit, creditLimit, calculatedBalance;
    
    do {
        printf("%s", "Enter account number (-1 to end): ");
        scanf("%li", &accountNumber);
        
        if (accountNumber == -1) { break; }
            
        printf("%s", "Enter beginning balance: ");
        scanf("%f", &initialBalance);
        printf("%s", "Enter total charges: ");
        scanf("%f", &totalDeposit);
        printf("%s", "Enter total credits: ");
        scanf("%f", &totalExpense);
        printf("%s", "Enter credit limit: ");
        scanf("%f", &creditLimit);
        
        calculatedBalance = initialBalance + totalDeposit - totalExpense;
        
        if (calculatedBalance > creditLimit) {
            puts("");
            printf("%s%li", "Account:            ", accountNumber);
            puts("");
            printf("%s%.2f","Credit limit:       ", creditLimit);
            puts("");
            printf("%s%.2f","Calculated balance: ", calculatedBalance);
            puts("");
        }
        puts("");
    } while (accountNumber != -1);
    return 0;
} // Main function - end
