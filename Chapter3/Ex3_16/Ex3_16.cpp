/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.16 - Page 106 - Gas Mileage                   |
  +----------------------------------------------------------------------+
  | Filename: Ex3_16.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>

// Main function - begin
int main ( void )
{
    float spentFuel = 0.0f;             // Spent fuel
    float distanceDriven = 0.0f;        // Driven distance
    
    float consumptionForOneFuelTank = 0.0f;     // Consumption for one fuel tank Lt/100km
    unsigned int fuelTankCounter = 0;           // Fuel tank counter
    float consumptionForAllFuelTanks = 0.0f;    // Consumption for all fuel tanks Lt/100km
    float averageConsumption = 0.0f;            // Average Consumption Lt/100km

    do {
        printf("%s", "Enter the liters used (-1 to end): ");
        scanf("%f", &spentFuel);

        if (spentFuel <= 0.0f) {
            break;
        }

        printf("%s", "Enter the kilometers driven: ");
        scanf("%f", &distanceDriven);

        if (distanceDriven <= 0.0f) {
            puts("Check the entered value!");
            continue;
        }

        consumptionForOneFuelTank = 100 * ( spentFuel / distanceDriven );
        consumptionForAllFuelTanks += consumptionForOneFuelTank;
        ++fuelTankCounter;

        printf("%s%f", "The Lt/100km for this tank was ", consumptionForOneFuelTank);
        puts("\n");
    } while ( spentFuel > 0.0f );

    if (fuelTankCounter > 0) {
        averageConsumption = consumptionForAllFuelTanks / fuelTankCounter;  
        printf("%s%f", "The overall average Lt/100km was ", averageConsumption);
    }
    

    return 0;
} // Main function - end
