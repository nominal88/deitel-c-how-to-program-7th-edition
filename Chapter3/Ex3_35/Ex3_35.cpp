/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.35 - Page 112 - Printing the equivalent of    |
  | a Binary Number.                                                     |
  +----------------------------------------------------------------------+
  | Filename: Ex3_35.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

// Definitions
#define DECIMAL    (10)
#define BINARY      (2)

// Includes
#include <stdio.h>

// Function prototypes
unsigned long powNumber(unsigned char number, unsigned char power);
unsigned char calculateNumOfDigits(unsigned long long int number);
unsigned long binaryToDecimal( unsigned long long int binary );

int main()
{
    unsigned long long readBin = 0;
    unsigned long decimalConv = 0;
    printf ("%s", "Enter the binary number: ");
    scanf("%llu", &readBin);

    decimalConv = binaryToDecimal(readBin);
    printf ("%s%lu\r\n", "The decimal equivalent: ", decimalConv);

    return 0;
}


unsigned long binaryToDecimal( unsigned long long int binary ) {
    unsigned char numOfDigits = calculateNumOfDigits(binary);
    unsigned char digit = 0;
    unsigned long multiplier = 1;
    unsigned long decimal = 0;

    for (unsigned char index = 0; index < numOfDigits; index++) {
        digit = binary % DECIMAL;

        if (digit > 1) {return 0;}

        multiplier = powNumber(2,index);

        decimal += multiplier * digit;

        binary /= DECIMAL;
    }

    return decimal;

}

unsigned long powNumber(unsigned char number, unsigned char power) {
    unsigned long multiplier = 1;


    for (unsigned char pow = 0; pow < power; pow++) {
        multiplier *= number;
    }

    return  multiplier;
}


unsigned char calculateNumOfDigits( unsigned long long int number )
{
    unsigned char numOfDigits = 0;

    while (number > 0) {
        numOfDigits++;
        number /= DECIMAL;
    }

    return numOfDigits;
}
