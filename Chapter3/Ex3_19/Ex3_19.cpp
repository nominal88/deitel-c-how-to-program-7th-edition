/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.19 - Page 108 - Interest Calculator           |
  +----------------------------------------------------------------------+
  | Filename: Ex3_19.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>

const unsigned int numberOfDaysOfOneYear = 365;

// Main function - begin
int main ( void )
{
    float principal, rate, interest;
    unsigned int days;

    do {
        printf("%s", "Enter load principal (-1 to end): ");
        scanf("%f", &principal);

        if (principal < 0) {
            break;
        }

        printf("%s", "Enter interest rate: ");
        scanf("%f", &rate);
        printf("%s", "Enter term of the loan in days: ");
        scanf("%u", &days);

        interest = principal * rate * days / numberOfDaysOfOneYear;

        printf("%s%.2f", "The interest charge is $", interest);
        puts("");
        puts("");
        
    } while (principal >= 0);

    return 0;
} // Main function - end