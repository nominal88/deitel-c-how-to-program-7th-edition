/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.36 - Page 112 - How fast is your computer?    |
  +----------------------------------------------------------------------+
  | Filename: Ex3_36.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/
#include <stdio.h>
#include <time.h>

int main()
{
    while (1) {
        unsigned long long int counter = 0;
        double time_spent = 0;

        clock_t begin = clock();

        while (counter < 1000000000) { counter++ ;}

        clock_t end = clock();

        time_spent = (double) (end - begin) / CLOCKS_PER_SEC;

        printf("%s%f\r\n", "Execution time: ", time_spent);

    }


    return 0;
}
