/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.18 - Page 108 - Sales Commission Calculator   |
  +----------------------------------------------------------------------+
  | Filename: Ex3_18.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>

// Function prototypes
float calculateTheSalary (float salesDollar);

// Main function - begin
int main ( void )
{
    float salesDollar;
    float salary;

    do {
        printf("%s", "Enter sales in dollars (-1 to end): ");
        scanf("%f", &salesDollar);

        if (salesDollar < 0) {
            break;
        }

        salary = calculateTheSalary(salesDollar);
        printf("%s%.2f", "Salary is: $", salary);
        puts("");
        puts("");
    } while (salesDollar >= 0);

    return 0;
} // Main function - end

// calculateTheSalary function - begin
float calculateTheSalary (float salesDollar)
{
    const float commission = float(9) / 100;   // %9
    const float fixedFee = 200;         //Dollar

    return fixedFee + (salesDollar * commission);
} // calculateTheSalary function - end
