/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.20 - Page 108 - Salary Calculator             |
  +----------------------------------------------------------------------+
  | Filename: Ex3_20.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>

int main()
{
    short workedHours;
    float workerHourlyRate;
    float salary;

    do {
            printf("Enter # of hours worked (-1 to end): ");
            scanf("%hd", &workedHours);

            if ( workedHours < 0) {
                break;
            }

            printf("Enter hourly rate of the worker ($00.00): ");
            scanf("%f", &workerHourlyRate);

            if ( workedHours >= 0 && workedHours <= 40) {
                salary = workedHours * workerHourlyRate;
            }
            else if (workedHours > 40) {
                salary  =  40 * workerHourlyRate;
                salary += (workedHours  - 40) * workerHourlyRate * (float) 1.5;
            }

            printf("Salary is $%.2f\r\n", salary);
            printf("\r\n");
    } while (salary >= 0);
    return 0;
}
