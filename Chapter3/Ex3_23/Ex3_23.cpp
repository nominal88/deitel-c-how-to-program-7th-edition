/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2019 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 3 - Exercise 3.23 - Page 109 - Find the largest number.      |
  +----------------------------------------------------------------------+
  | Filename: Ex3_23.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>
#include <limits.h>

const unsigned int numberOfNumbers = 10;

int main()
{
    int actualNumber = 0;
    int largestNumber = INT_MIN;
    unsigned int index = 0;

    do {
        printf ("Number[%d]: ", index);
        scanf("%d", &actualNumber);

        if (actualNumber > largestNumber) {
            largestNumber = actualNumber;
        }

    } while (++index < numberOfNumbers);

    printf("The largest number is %d\r\n", largestNumber);

    return 0;
}
