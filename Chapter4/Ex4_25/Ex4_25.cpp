/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2020 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 4 - Exercise 4.25 - Page 155 - Table of Decimal, Binary,     |
  | Octal and Hexadecimal Equivalents.                                   |
  +----------------------------------------------------------------------+
  | Filename: Ex4_25.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define TABLE_MIN (0)
#define TABLE_MAX (256)

#define calcNumOfBits(decimal) log2((double)decimal)


void DecimalToBinaryConversation ( unsigned int decimalNumber, char **binaryValue );

int main()
{
    printf("%10s %7s %7s %11s\r\n","Binary", "Octal", "Decimal", "Hexadecimal");

    for (unsigned int counter = TABLE_MIN; counter <= TABLE_MAX; counter++) {
        char *pBinaryData = (char *)calloc(calcNumOfBits(TABLE_MAX) + 1, sizeof (char));
        memset(pBinaryData, '0', calcNumOfBits(TABLE_MAX)*sizeof(char));
        DecimalToBinaryConversation(counter, &pBinaryData);
        printf("%11s %5o %6u %9X\r\n", pBinaryData, counter, counter, counter);
        free(pBinaryData);
    }
    return 0;

}


void DecimalToBinaryConversation ( unsigned int decimalNumber, char **binaryValue )
{
    unsigned char numOfBits = calcNumOfBits(TABLE_MAX);

    if (decimalNumber == 0) {
        *(*binaryValue + numOfBits) = (char)(decimalNumber % 2) + '0';
    }
    else {
        while( decimalNumber > 0 ) {
            *(*binaryValue + numOfBits) = (char)(decimalNumber % 2) + '0';
            numOfBits--;
            decimalNumber /= 2;
        }
    }
}
