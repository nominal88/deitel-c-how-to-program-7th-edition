/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2020 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 4 - Exercise 4.32 - Page 156 - Modified Diamond-Printing     |
  | Program                                                              |
  +----------------------------------------------------------------------+
  | Filename: Ex4_32.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/
#include <stdio.h>

#define RHOMBUS_MAX_SIZE (7)

void drawRhombus (const unsigned int size);

int main()
{
    unsigned int rhombusSize = 0;
    do {
        printf("%s","Please enter the rhombus size(Odd number): ");
        scanf("%u", &rhombusSize);

        if(rhombusSize % 2 == 0) {
            puts("You had entered a even number. Please enter the odd number!");
        }

    } while (rhombusSize % 2 == 0);
    drawRhombus(rhombusSize);
    return 0;
}

void drawRhombus (const unsigned int size) {
    unsigned int centerPoint = size / 2 + 1;

    for (unsigned int row = 1; row <= size; row++) {
        unsigned int startPointIndex = (row <= centerPoint) ?
                    centerPoint - (row - 1) : (row - centerPoint + 1);
        unsigned int endPointIndex = (row <= centerPoint) ?
                    centerPoint + (row - 1) : (size -(row - centerPoint));
        for (unsigned int col = 1; col <= size; col++) {
            if ( (col >= startPointIndex)
              && (col <= endPointIndex ) ) {
                printf("*");
            }
            else {
                printf(" ");
            }
        }
        printf("\r\n");
    }


}
