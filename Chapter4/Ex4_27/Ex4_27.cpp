/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2020 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 4 - Exercise 4.27 - Page 155 - Pythagorean Triples           |
  +----------------------------------------------------------------------+
  | Filename: Ex4_27.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/
#include <stdio.h>

#define MAX_SIDE_LENGTH (5000)

int main()
{
    printf("%8s%8s%13s\r\n", "Side1", "Side2", "Hypotenuse");
    for (unsigned int side1 = 1; side1 < MAX_SIDE_LENGTH; side1++ ) {
        for (unsigned int side2 = 1; side2 < MAX_SIDE_LENGTH; side2++ ) {
            for (unsigned int hypotenuse = 1; hypotenuse < MAX_SIDE_LENGTH; hypotenuse++ ) {
                if( (side1 * side1 + side2 * side2) == (hypotenuse * hypotenuse) )
                        printf ("%7u%8u%10u\r\n", side1, side2, hypotenuse);
            }
        }
    }
    return 0;
}
