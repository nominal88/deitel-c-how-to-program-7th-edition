/*
  +----------------------------------------------------------------------+
  | Deitel - C How to program 7th edition                                |
  +----------------------------------------------------------------------+
  | Copyright (c) 2020 Nomin@L                                           |
  +----------------------------------------------------------------------+
  |    This project includes problem solutions of the Deitel's C Book    |
  | "C How to program 7th edition".                                      |
  +----------------------------------------------------------------------+
  | Chapter 4 - Exercise 4.26 - Page 155 - Calculating the Value of PI   |
  +----------------------------------------------------------------------+
  | Filename: Ex4_26.cpp                                                 |
  +----------------------------------------------------------------------+
  | Author: Erkan Ozvatan  <nominal88@hotmail.com>                       |
  +----------------------------------------------------------------------+
*/

#include <stdio.h>

#define MAX_NUM_OF_PI_ITERATION (1000000000)

double calculatePi(const unsigned int calcIteration);

int main()
{
    printf("%s%28s\r\n", "Iteration", "Number of PI Iteration");
    for(unsigned int iteration = 1; iteration <= MAX_NUM_OF_PI_ITERATION; iteration++) {
        if(iteration % 100000000 == 0) {
            printf("%6u%32.22f\r\n", iteration, calculatePi(iteration));
        }
    }
    return 0;
}

double calculatePi(const unsigned int calcIteration)
{
    const double PI_CALC_VALUE = 4;
    double pi = 0;

    for(unsigned int iteration = 1; iteration <= calcIteration; iteration++) {
        double convertedIterToCoef = (double)iteration * 2 - 1;
        double calculatedIteration = PI_CALC_VALUE / convertedIterToCoef;

        if( iteration % 2 == 1 ) {
            pi += calculatedIteration;
        }
        else {
            pi -= calculatedIteration;
        }
    }

    return  pi;
}
